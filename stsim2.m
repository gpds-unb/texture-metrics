function metric = stsim2(a, b)
  addpath(genpath('./matlab_pyramids_tools'));
  addpath(genpath('./stsim_lib'));
  
  ststm1 = stsim(a, b);
  
  [pa,pinda,steermtx,harmonics] = buildSFpyr(a);
  [pb,pindb,steermtx,harmonics] = buildSCFpyr(b);

  pyrA = getlist(pa, pinda);
  pyrB = getlist(pb, pindb);

  metric1 = orientation_invariant(pyrA, pyrB);
  metric2 = scale_invariant(pyrA, pyrB);
  metric = mean([ststm1 metric1 metric2]);
end

function metric = scale_invariant(pyrA, pyrB)
  elements = numel(pyrA);
  
  temp = 0;
  count = 0;

  for scale=1:elements
    x = pyrA{scale};
    y = pyrB{scale};
    Nor = 4;
    [x, y] = resize_scale_2(x, y);
    for orient=1:Nor-1
        im11 = abs(x(:, orient));
        im21 = abs(y(:, orient));
        parfor orient2=orient+1:Nor
           im12 = abs(x(:, orient2));
           im22 = abs(y(:, orient2));
           crossmap = compute_cross_term(im11, im21, im12, im22);
           temp = temp + mean(crossmap);
           count = count + 1;
        end
    end
  end
  metric = temp / (count + realmin);
end

function metric = orientation_invariant(pyrA, pyrB)
  elements = numel(pyrA);
  
  temp = 0;
  count = 0;
  
  for scale=2:elements
    x1 = pyrA{scale - 1};
    x2 = pyrA{scale};
    y1 = pyrB{scale - 1};
    y2 = pyrB{scale};
    Nor = 4;
    for orient=1:Nor
      [x1, x2, y1, y2] = resize_scale(x1, x2, y1, y2);
      im11 = abs(x1(:, orient));
      im12 = abs(x2(:, orient));
	  im21 = abs(y1(:, orient));
	  im22 = abs(y2(:, orient));
      crossmap = compute_cross_term(im11, im12, im21, im22);
      temp = temp + mean(crossmap);
      count = count + 1;
    end
  end
  metric = temp / (count + realmin);
end


function [im11, im12] = resize_scale_2(im11, im12)
  [M11 N11] = size(im11);
  [M12 N12] = size(im12);
  M = max([M11 M12]);
  N = max([N11 N12]);
  
  im11 = imresize(im11, [M N]);
  im12 = imresize(im12, [M N]);
end

function [im11, im12, im21, im22] = resize_scale(im11, im12, im21, im22)
  [M11 N11] = size(im11);
  [M12 N12] = size(im12);
  [M21 N21] = size(im11);
  [M22 N22] = size(im12);
  M = max([M11 M12 M21 M22]);
  N = max([N11 N12 N21 N22]);
  
  im11 = imresize(im11, [M N]);
  im12 = imresize(im12, [M N]);
  im21 = imresize(im21, [M N]);
  im22 = imresize(im22, [M N]);
end

function crossmap = compute_cross_term(im11, im12, im21, im22)
  C = 0.001;
  win = 7;
  window2 = (1.0 ./ (win .^ 2)) .* ones(win, win);
 
  mu11 = convs(im11, window2);
  mu12 = convs(im12, window2);
  mu21 = convs(im21, window2);
  mu22 = convs(im22, window2);
  
  sigma11_sq = convs(abs(im11 .* im11), window2) - abs(mu11 .* mu11);
  sigma12_sq = convs(abs(im12 .* im12), window2) - abs(mu12 .* mu12);
  sigma21_sq = convs(abs(im21 .* im21), window2) - abs(mu21 .* mu21);
  sigma22_sq = convs(abs(im22 .* im22), window2) - abs(mu22 .* mu22);
  
  sigma1_cross = convs(im11 .* im12, window2) - abs(mu11 .* mu12);
  sigma2_cross = convs(im21 .* im22, window2) - abs(mu21 .* mu22);
  
  rho1 = (sigma1_cross + C) ./ (sqrt(sigma11_sq) .* sqrt(sigma12_sq) + C);
  rho2 = (sigma2_cross + C) ./ (sqrt(sigma21_sq) .* sqrt(sigma22_sq) + C);
  
  crossmap = 1.0 - 0.5 .* abs(rho1 - rho2);
end
