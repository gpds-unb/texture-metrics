function [ straight ] = getlist(pyr, pind)
  straight = {};
  nind = size(pind,1);
  for bnum = 1:nind
      band = pyrBand(pyr,pind,bnum);
      straight{bnum} = band;
  end
end

