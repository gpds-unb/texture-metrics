function [ pool ] = pooling(im1, im2)
  win = 7;
  baseL = compute_L_term(im1, im2, win);
  baseC = compute_C_term(im1, im2, win);
  baseC01 = compute_C01_term(im1, im2, win);
  baseC10 = compute_C10_term(im1, im2, win);
  
  tmp = (baseL .* baseC .* baseC01 .* baseC10) .^ (1.0 / 4.0);
  pool = mean(tmp(:));
end

function Lmap = compute_L_term(im1, im2, win)
  C = 0.001;
  window = fspecial('gaussian', win, win/6);
  mu1 = abs(convs(im1, window));
  mu2 = abs(convs(im2, window));
  Lmap = (2 .* mu1 .* mu2 + C) ./ (mu1 .* mu1 + mu2 .* mu2 + C);
end

function Cmap = compute_C_term(im1, im2, win)
  C = 0.001;
  window = fspecial('gaussian', win, win/6);
  mu1 = abs(convs(im1, window));
  mu2 = abs(convs(im2, window));
  sigma1_sq = convs(abs(im1 .* im1), window) - mu1 .* mu1;
  sigma1 = sqrt(sigma1_sq);
  sigma2_sq = convs(abs(im2 .* im2), window) - mu2 .* mu2;
  sigma2 = sqrt(sigma2_sq);
  Cmap = (2 .* sigma1 .* sigma2 + C) ./ (sigma1_sq + sigma2_sq + C);
end

function C01map = compute_C01_term(im1, im2, win)
  C = 0.001;
  window2 = 1 ./ (win .* (win - 1)) .* ones(win, win-1);
  
  [M1, N1] = size(im1);
  [M2, N2] = size(im2);
  
  im11 = zeros(M1, N1);
  im12 = zeros(M1, N1);
  im21 = zeros(M2, N2);
  im22 = zeros(M2, N2);
  
  im11(:, 1:end-1) = im1(:, 1:end-1);
  im12(:, 2:end) = im1(:, 2:end);
  im21(:, 1:end-1) = im2(:, 1:end-1);
  im22(:, 2:end) = im2(:, 2:end);
  
  mu11 = convs(im11, window2);
  mu12 = convs(im12, window2);
  mu21 = convs(im21, window2);
  mu22 = convs(im22, window2);
  
  sigma11_sq = convs(abs(im11.*im11), window2) - abs(mu11.*mu11);
  sigma12_sq = convs(abs(im12.*im12), window2) - abs(mu12.*mu12);
  sigma21_sq = convs(abs(im21.*im21), window2) - abs(mu21.*mu21);
  sigma22_sq = convs(abs(im22.*im22), window2) - abs(mu22.*mu22);
  
  sigma1_cross = convs(im11 .* conj(im12), window2) - mu11 .* conj(mu12);
  sigma2_cross = convs(im21 .* conj(im22), window2) - mu21 .* conj(mu22);
  
  rho1 = (sigma1_cross + C) ./ (sqrt(sigma11_sq) .* sqrt(sigma12_sq) + C);
  rho2 = (sigma2_cross + C) ./ (sqrt(sigma21_sq) .* sqrt(sigma22_sq) + C);
  C01map = 1 - 0.5 .* abs(rho1 - rho2);
end

function C10map = compute_C10_term(im1, im2, win)
  C = 0.001;
  window2 = 1 ./ (win .* (win - 1)) .* ones(win-1, win);

  [M1, N1] = size(im1);
  [M2, N2] = size(im2);
  
  im11 = zeros(M1, N1);
  im12 = zeros(M1, N1);
  im21 = zeros(M2, N2);
  im22 = zeros(M2, N2);
  
  im11(1:end-1, :) = im1(1:end-1, :);
  im12(2:end, :) = im1(2:end, :);
  im21(1:end-1, :) = im2(1:end-1, :);
  im22(2:end, :) = im2(2:end, :);
  
  mu11 = convs(im11, window2);
  mu12 = convs(im12, window2);
  mu21 = convs(im21, window2);
  mu22 = convs(im22, window2);
  
  sigma11_sq = convs(abs(im11 .* im11), window2) - abs(mu11 .* mu11);
  sigma12_sq = convs(abs(im12 .* im12), window2) - abs(mu12 .* mu12);
  sigma21_sq = convs(abs(im21 .* im21), window2) - abs(mu21 .* mu21);
  sigma22_sq = convs(abs(im22 .* im22), window2) - abs(mu22 .* mu22);
  
  sigma1_cross = convs(im11 .* conj(im12), window2) - mu11 .* conj(mu12);
  sigma2_cross = convs(im21 .* conj(im22), window2) - mu21 .* conj(mu22);

  rho1 = (sigma1_cross + C) ./ (sqrt(sigma11_sq) .* sqrt(sigma12_sq) + C);
  rho2 = (sigma2_cross + C) ./ (sqrt(sigma21_sq) .* sqrt(sigma22_sq) + C);
  C10map = 1 - 0.5 .* abs(rho1 - rho2);
end