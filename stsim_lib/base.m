function [ log_rad, angle ] = base(m, n)
  x = create_x(m);
  y = create_y(n);
  [xv, yv] = create_meshgrid(x, y);
  angle = atan2(yv, xv);
  log_rad = compute_rad(xv, yv, m, n);
end

function x = create_x(m)
  start = -1;
  stop = 1 - floor((1 - mod(m, 2)) * 2 / m); 
  x = linspace(start, stop, m);
end

function y = create_y(n)
  start = -1;
  stop = 1 - floor((1 - mod(n, 2)) * 2 / n);
  y = linspace(start, stop, n);
end

function [xv, yv] = create_meshgrid(x, y)
  [xv, yv] = meshgrid(y, x);
end

function log_rad = compute_rad(xv, yv, m, n)
  rad = sqrt(xv .^ 2 + yv .^ 2);
  rad(floor(m/2), floor(n/2)) = rad(floor(m/2), floor(n/2) - 1);
  log_rad = log2(rad);
end