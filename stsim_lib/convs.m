function [ c ] = convs(im, filt)
  c = imfilter(im, filt, 'symmetric', 'same', 'corr');
end

