clear;
clc;

a = imread('1.2.02.tiff');
b = imread('1.2.09.tiff');
c = imread('1.1.08.tiff');


v1 = stsim(a, b);
v2 = stsim2(a, b);

u1 = stsim(a, c);
u2 = stsim2(a, c);

z1 = stsim(c, b);
z2 = stsim2(c, b);

disp([v1 v2 u1 u2 z1 z2])