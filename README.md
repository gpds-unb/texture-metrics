Contents
========

1.  [Description](#description)
2.  [License](#license)


Description
===========

This code implements metrics to compare distance between texture images.


Requirements
============

This project requires [Matlab Pyramids Toolbox](https://gitlab.com/gpds-unb/matlabpyrtools-redistribution).


License
=======

This project is licensed under [The MIT License](https://opensource.org/licenses/MIT)