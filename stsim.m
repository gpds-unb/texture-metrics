function [ metric ] = stsim(a, b)
  addpath(genpath('./matlab_pyramids_tools'));
  addpath(genpath('./stsim_lib'));
  [pa,pinda,steermtx,harmonics] = buildSFpyr(a);
  [pb,pindb,steermtx,harmonics] = buildSCFpyr(b);

  pyrA = getlist(pa, pinda);
  pyrB = getlist(pb, pindb);

  elements = numel(pyrA);

  metric = 0;

  for i=1:elements
      x = pyrA{i};
      y = pyrB{i};
      metric = metric + pooling(x,y);
  end

  metric = metric / (elements + realmin);
end
